import re
import pytest
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options

from pages.login_page import LoginPage
from pages.page import Page
from pages.todolists_page import ToDoListsPage

driver = None


def setup_module(module):
    global driver
    chrome_options = Options()
#    chrome_options.add_argument("--headless")
    chrome_options.add_experimental_option("detach", True)
    driver = webdriver.Chrome(options=chrome_options)
    driver.get("http://django-todo.org")
    login_page = LoginPage(driver)


def click_on_first_login():
    try:
        login_btn = driver.find_element_by_xpath('//a[text()="Log In"]')
        login_btn.click()
    except NoSuchElementException:
        pass


def click_on_second_login():
    login_btn1 = driver.find_element_by_xpath('//button[text()="Login"]')
    login_btn1.click()


def get_current_user():
    xpath = '//footer//span[contains(text(),"django-todo")]'
    try:
        footer_elem = driver.find_element_by_xpath(xpath)
        m = re.search(r'Logged in as (\w+)', footer_elem.text)
        if not m:
            return None
        return m.group(1)
    except NoSuchElementException:
        assert False, f'WARNING! Check your text in footer "{xpath}"'


def teardown_function(function):
    try:
        logout_link = driver.find_element_by_xpath('//a[text()="Log Out"]')
        logout_link.click()
    except NoSuchElementException:
        pass


@pytest.mark.parametrize("username,password,expected_current_user", [
    ("staffer", "todo", "staffer"),
    ("user1", "todo", "user1"),
    ("user2", "todo", "user2"),
    ("user3", "todo", "user3"),
    ("user4", "todo", "user4"),
    ("user1", "", None),
    ("", "", None),
    ("", "todo", None),
    ("", "wtf", None),
    ("user1", "", None),
    ("user1", "wtf", None),
    ("user2", "", None),
    ("user2", "wtf", None),
    ("user3", "", None),
    ("user3", "wtf", None),
    ("user4", "", None),
    ("user4", "wtf", None),
    ("staffer", "wtf", None),
    ("staffer", "", None),
])
def test_login(username, password, expected_current_user):
    click_on_first_login()
    user_name_elem = driver.find_element_by_id('id_username')
    user_name_elem.send_keys(username)
    password_elem = driver.find_element_by_id('id_password')
    password_elem.send_keys(password)
    click_on_second_login()

    # todolist_page = ToDoListsPage(driver)
    # titles = todolist_page.get_lists()
    # for title in titles:
    #     todolist_page.open_list_item(title.text)

    assert get_current_user() == expected_current_user

