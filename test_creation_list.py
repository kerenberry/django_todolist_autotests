import pytest
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options

from pages.login_page import LoginPage

driver = None


def setup_function(function):
    global driver
    chrome_options = Options()
#    chrome_options.add_argument("--headless")
    chrome_options.add_experimental_option("detach", True)
    driver = webdriver.Chrome(options=chrome_options)
    driver.get("http://django-todo.org/login")
    login_page = LoginPage(driver)
    login_page.login('staffer', 'todo')


def test_creation_new_list():
    new_list_btn = driver.find_element_by_xpath('//a[text()="Create new todo list"]')
    new_list_btn.click()
    list_name_elem = driver.find_element_by_id('id_name')
    list_name_elem.send_keys("wtf_list")

    assert