from pages.page import Page


class LoginPage(Page):
    def __init__(self, driver):
        super(LoginPage, self).__init__(driver)

    def login(self, username, password):
        user_name_elem = self._driver.find_element_by_id('id_username')
        user_name_elem.send_keys(username)
        password_elem = self._driver.find_element_by_id('id_password')
        password_elem.send_keys(password)
        login_btn = self._driver.find_element_by_xpath('//button[text()="Login"]')
        login_btn.click()
