from pages.page import Page


class ToDoListsPage(Page):
    def __init__(self, driver):
        super(ToDoListsPage, self).__init__(driver)

    def create_new_list(self):
        create_btn = self._driver.find_element_by_xpath('//button[contains(text(),"Create")]')
        create_btn.click()

    # def get_groups(self):
    #     groups = self._driver.find_elements_by_xpath('//h3')
    #     return groups

    def get_lists(self):
        lists = self._driver.find_elements_by_xpath("//li[contains(@class,'list-group-item')]//a")
        return lists

    def open_list_item(self, title):
        list_title = self._driver.find_element_by_xpath(f'//a[text()="{title}"]')
        list_title.click()

#("//ul[contains(@class,'list-group')]")