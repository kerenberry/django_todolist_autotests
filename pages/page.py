class Page():
    def __init__(self, driver):
        self._driver = driver

    from enum import Enum
    class PageType(Enum):
        LOGIN_PAGE = 1
        LISTS_PAGE = 2
        ADD_LIST_PAGE = 3

    @staticmethod
    def goto(page_type):
        if page_type == LOGIN_PAGE:
            driver.get("http://django-todo.org/login")

